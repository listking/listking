import {print, unauthenticatedPostRequest, getRequest, postRequest, deleteRequest} from './functions.js';
import {baseUrl, debug} from './configuration.js';

var elList, addLink, newEl, counter, listItems;         // Declare variables
let globalResponse, globVal;                            // declare debug variables
const listId = 4;                           // "grocery list"; add functionality to select list later

elList = document.getElementById('shoppingList');       // Get list
addLink = document.querySelector('.add');               // Get add item button
// print(addLink.outerHTML);
counter = document.getElementById('counter');           // Get item counter

function addItem(event) {                                   
  event.preventDefault();                             // Prevent link action
  newEl = document.createElement('li');               // New <li> element
  newEl.setAttribute('class', 'hot');
  let newA = document.createElement('a');             // new <a> element
  let item = document.getElementById('item').value;
  let newText = document.createTextNode(item);
  print("item value: " + item);
  let elMsg = document.getElementById('submit-feedback');
  postRequest('/items', { 'title': item,
                          'isComplete': false,
                          'itemListId': listId
  })
    .then(response => {
      elMsg.textContent = '';
      elMsg.setAttribute('style', '');
      newA.appendChild(newText);                          // Add text to <a>
      newA.setAttribute('href', '/items/' + response.id);
      
      let newDiv = document.createElement('div');
      newDiv.setAttribute('class', 'delete');   
      if(response.isComplete === true){
        newEl.setAttribute('class', 'complete');
      }else{        
        newDiv.setAttribute("style", "display: none");
        newEl.setAttribute('class', 'hot');
      }
      newEl.appendChild(newA);                            // add <a> to <li>
      newEl.appendChild(newDiv);  
      elList.appendChild(newEl);                          // Add <li> to list            
    })
    .catch(error => {
      print(error);
      elMsg.textContent = 'error saving';
      elMsg.setAttribute('style', 'background-color: red;');
      globalResponse = error;
    })
  document.getElementById('item').value = "";         // clear input field
  document.getElementById('item').focus();            // focus input field
}
function updateCount() {                                
  listItems = elList.getElementsByTagName('li').length;  // Get total of <li>s
  counter.innerHTML = listItems;                      // Update counter
}
function itemDone(event) {                                  
//     print("function itemDone");
  event.preventDefault();                             // Prevent the link from taking you elsewhere
  var target, elListItem;                                          // Declare variables
  target = event.target;                              // Get the item clicked link
//     print("nodeName: ", target.nodeName);
  if(target.nodeName.toLowerCase() == "div"){  
    elListItem = target.parentNode;             
    try{
      print("trying to delete item ...");
//             globVal = item;                                     // debugging
      let endpoint = elListItem.firstElementChild.getAttribute('href');
      print("href: " + endpoint);
      deleteRequest(endpoint)
        .then(
          elList.removeChild(elListItem),
          updateCount()
        );
    }catch(error){
      print(error);
    }
  }else{
    if(target.nodeName.toLowerCase() == "a"){           // If user clicked on an a element
      elListItem = target.parentNode;                 // Get its li element
    }else if(target.nodeName.toLowerCase() == "em"){    // If the user clicked on an em element
      elListItem = target.parentNode.parentNode;   
    }else if(target.nodeName.toLowerCase() == "li"){  
      elListItem = target; 
    }
//         globVal = elListItem;
    let div = elListItem.getElementsByTagName("div")[0];
    let isComplete;
    if(elListItem.className === "hot"){
//             print("clicked hot item");
      elListItem.setAttribute("class", "complete");    
      div.setAttribute("style", "");
      isComplete = true;
    }else if(elListItem.className === "complete"){
      elListItem.setAttribute("class", "hot");
      div.setAttribute("style", "display: none");
      isComplete = false;
    }
    completeRequest(elListItem, isComplete)    
  }
}

// -------------------------- API calls ----------------------------------- //
async function completeRequest(liEl, isComplete){
  print("trying to toggle...");
  globVal = liEl;
  print("isComplete: " + isComplete);
  let href = liEl.firstElementChild.getAttribute('href');
  let id = href.slice(7);
  print("id: " + id);
  let token = localStorage.getItem("token");
  let rawResponse = await fetch(baseUrl + '/items/' + id, {
    method: 'PATCH', 
    body: JSON.stringify({
      'isComplete': isComplete,
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  });
  print(rawResponse);
  if(!rawResponse.ok){
    print("error on PATCH");
    throw new Error(rawResponse.error);
  }else{ 
//         return await rawResponse.json();
    print("successfully toggled isComplete");
  }
}
async function addItemsFromDB(){                        
  let endpoint = '/item-lists/' + listId + '/items';
  print("trying to add items from DB...");
  try{
    let data = await getRequest(endpoint);
    data.forEach(item => { 
      newEl = document.createElement('li');            
      let newA = document.createElement('a');
      let newText = document.createTextNode(item.title);
      newA.appendChild(newText);                      // Add text to <a>
      newA.setAttribute('href', '/items/' + item.id);
      newEl.appendChild(newA);                        // add <a> to <li>
      let newDiv = document.createElement('div');
      newDiv.setAttribute('class', 'delete');   
      if(item.isComplete === true){
        newEl.setAttribute('class', 'complete');
      }else{        
        newDiv.setAttribute("style", "display: none");
        newEl.setAttribute('class', 'hot');
      }
      newEl.appendChild(newDiv);  
      elList.appendChild(newEl);
    });
  }catch{  
    print("script.js: probably not authenticated");
    window.location.href = ('login.html');
  }
}

// -------------------------- main section ---------------------------- //
addItemsFromDB();
updateCount();
document.getElementById('item').value = "";
addLink.addEventListener('click', addItem, false);      // Click on button
elList.addEventListener('DOMNodeInserted', updateCount, false); // DOM updated
elList.addEventListener(
  'click', 
  event => { itemDone(event); },
  false                                               // Use bubbling phase for flow
);
document.getElementById('item').focus();

