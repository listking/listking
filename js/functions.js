import {baseUrl, debug} from './configuration.js';

let glob;

function print(msg){
  if(debug === true){
    console.log(msg);
  }
}
async function unauthenticatedPostRequest(endpoint, bodyData){
  let rawResponse = await fetch(baseUrl + endpoint, {
    method: 'POST', 
    body: JSON.stringify(bodyData),
    headers: {
      'Content-Type': 'application/json'
    }
  });
//     print(rawResponse);
  if(rawResponse.status === 422){
    print("wrong password");
    print(rawResponse);
    throw new Error(rawResponse.error);
  }else if(!rawResponse.ok){
    print("error");
    print(rawResponse);
    throw new Error(rawResponse.error);
  }else{ 
    return await rawResponse.json();
  }
}
async function getRequest(endpoint){
  let token = localStorage.getItem("token");
  let rawResponse = await fetch(baseUrl + endpoint, {
    method: 'GET', 
    headers: {
      'Authorization': 'Bearer ' + token
    }
  });
  if(rawResponse.status === 422){
    print("wrong password or not authenticated yet");
    print(rawResponse);
    throw new Error(rawResponse.error);
  }else if(!rawResponse.ok){
    print("error");
    print(rawResponse);
    throw new Error(rawResponse.error);
  }else{ 
    return await rawResponse.json();
  }
}
async function postRequest(endpoint,data){
  let token = localStorage.getItem("token");
  let rawResponse = await fetch(baseUrl + endpoint, {
    method: 'POST', 
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  });
  print(rawResponse);
  if(!rawResponse.ok){
    print("error on POST");
    throw new Error(rawResponse.error);
  }else{ 
    return await rawResponse.json();
  }
}

async function deleteRequest(endpoint){
  let token = localStorage.getItem("token");
  let rawResponse = await fetch(baseUrl + endpoint, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  });
//     print(rawResponse);
  if(!rawResponse.ok){
    print("error on DELETE");
    throw new Error(rawResponse.error);
  }else{ 
    print("successful DELETE");
  }
}
export {print, unauthenticatedPostRequest, getRequest, postRequest, deleteRequest};
