import {print, unauthenticatedPostRequest, getRequest} from './functions.js';
import {baseUrl, debug} from './configuration.js';

let glob;                                              // global variable for debugging

function checkUsername() {                             // Declare function
  var elMsg = document.getElementById('feedback');     // Get feedback element
  if (this.value.length < 5) {                         // If username too short
    elMsg.textContent = 'Username must be 5 characters or more'; // Set msg
    print("username too short");
  } else {                                             // Otherwise
    elMsg.textContent = '';                            // Clear msg
    elMsg = document.getElementById('submit-feedback');
    elMsg.textContent = '';
  }
}
function submit(event){
  event.preventDefault();                              // prevent submitting
  var name = document.getElementById('username').value;
  var passwd = document.getElementById('password').value;
  let elMsg = document.getElementById('submit-feedback');
  if(name.length >= 5 && passwd !== "") {
    unauthenticatedPostRequest('/users/login', { 'email': name, 'password': passwd } )
      .then(resp => {
//         globVal = resp;
        if(resp.token){
          print("credentials OK, trying to redirect ...");
          localStorage.setItem("token", resp.token);  // change to sessionStorage in production
//           window.location.href = ('file:///home/rynnon/workspace/listking/index.html');
          window.location.href = ('index.html');
          getRequest("/users/me")
            .then(resp => {
              print("email: " + resp.email)
            });
        }
      })
      .catch(error => {
        elMsg.textContent = "wrong username or password";
        print(error);
      })
  }else if(name < 5){
    elMsg.textContent = 'cannot log in with invalid username';
  }else if(passwd === ""){
    elMsg.textContent = 'cannot log in without password';
  }else{
    print("error handling username/password input");
  }
}

var elUsername = document.getElementById('username');  // Get username input
// When it loses focus call checkUsername()
elUsername.addEventListener('blur', checkUsername, false);

var elSubmit = document.getElementById('submit');
elSubmit.addEventListener('click', event => { submit(event) }, false);
// document.getElementById('form').addEventListener('submit', '#form', function(event){
//     event.preventDefault();
// });
