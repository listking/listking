describe('Login page', () => {

    it('shows the login form', () => {
        cy.viewport(550, 700)
        cy.visit('/login');
        cy.contains('username');
        cy.contains('log in!')
    })
    it('does not send request to backend with invalid fields', () => {
        cy.viewport(550, 700)
        cy.visit('/login')
        
        cy.get('#username').type('12345')
        cy.get('#password').type('6890')
        cy.get('#submit').click()
        cy.contains('invalid password')

        cy.get('#username').clear()
        cy.get('#password').clear()
        cy.get('#username').type('1234')
        cy.get('#password').type('67890')
        cy.get('#submit').click()
        cy.contains('invalid username')
    })
    it('displays correct error message when credentials are valid but wrong', () => {
        cy.viewport(550, 700)
        cy.intercept('/**')
        cy.intercept('POST', '/users/login', {
            statusCode: 422,
            body: {"error":{
                "statusCode":422,
                "name":"mockUnprocessableEntityError",
                "message":"The request body is invalid. See error object `details` property for more info.",
                "code":"VALIDATION_FAILED",
                "details":[{"path":"/email","code":"format","message":"should match format \"email\"","info":{"format":"email"}}]
            }}
        })
        cy.visit('/login')
        cy.get('#username').clear()
        cy.get('#password').clear()
        cy.get('#username').type('12345')
        cy.get('#password').type('67890')
        cy.get('#submit').click()
        cy.contains('wrong username or password')
    })
    it('redirects to Lists when credentials are correct', () => {
        cy.viewport(550, 700)
        cy.visit('/login')
        cy.get('#username').clear()
        cy.get('#password').clear()
        cy.get('#username').type('user4@example.com')
        cy.get('#password').type('DHz5fznHskgte3')
        cy.get('#submit').click()
        cy.get('#page>h2').should('contain', 'Lists')
    })
    
})