
describe('Lists page', () => {
    
    beforeEach(() => {
        cy.login('user4@example.com', 'DHz5fznHskgte3')
        cy.visit('/lists')
    })
    it('displays lists', () => {
        cy.get('ul>li>a>span')
            // make sure these lists are actually in the database and isn't renamed
            .should('contain', 'Liste 1-3')
            .should('contain', 'äöööö')
    })
    it('adds list item', () => {
        cy.get('#item').type('test cypress')
        cy.get('#add-item').click()
        cy.wait(200)
        cy.get('ul>li').last()
            .should('contain', 'test cypress')
    })
    it('cancels updating list title', () => {
        cy.get('ul>li').contains('test cypress').parent().contains('edit')
            .click()
        cy.get('#edit-item').type(' 1')
        cy.get('#edit-stop').click()
        cy.get('ul>li').should('not.contain', 'test cypress 1')
    })
    it('updates list title', () => {
        cy.get('ul>li').contains('test cypress').parent().contains('edit')
            .click()
        cy.get('#edit-item').type(' 1')
        cy.get('#update-item').click()
        cy.get('ul>li').contains('test cypress 1')
    })
    it('deletes list item', () => {
        cy.get('ul>li>a>span').last().invoke('text')
            .should('contain', 'test cypress 1')
        cy.get('ul>li>a>span').last().parent().parent().find('.delete')
            .click() // displays an alert that cypress silently clicks 'ok' on
        cy.wait(200)
        cy.get('ul>li>a>span').last().invoke('text')
            .should('not.contain', 'test cypress')
    })

})