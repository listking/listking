# List King (React version)

### What is this?                          

This is the React version of a rebuild of an example app from the book "JavaScript & JQuery" by Jon Duckett (http://www.javascriptbook.com/). It uses assets distributed with the book. The backend's repository is https://bitbucket.org/listking/listking-backend.

See also https://bitbucket.org/listking/listking/src/react-projekt/README.md

### What does it do?

Displays a list of lists of items, meant to represent to do lists, shopping lists or similar. Optimized for mobile devices.

### screenshots

login

![login](../screenshots/2021-09-26-153208_631x666_scrot.png?raw=true "login")

list of lists

![lists](../screenshots/2021-09-26-153351_603x637_scrot.png?raw=true "lists")

list of todo-items

![list](../screenshots/2021-09-26-153404_584x844_scrot.png?raw=true "list")

### architectural overview

Uses React functions and the hooks API (instead of classes), react-browser-router for routing, and react-redux and axios for API interaction. CSS created with SCSS. Uses Nginx to serve files in "production" mode. The backend was created with Loopback's scaffolding CLI and uses a JSON-file for data storage. See http://51.15.88.204:3000/explorer for API documentation. Public live prototype running on a VPS hosted by Scaleway. See [broad todo](#broad-todo) for TBD items.

### installing

Use install.sh to install both backend and frontend. Written and tested for Ubuntu 20.04. You will probably need to adjust URLs in react-listking/src/assets/base_url.js and react-listking/package.json to match your host if you want to serve the app with a webserver.

### access production build Abraxas
```bash
cd /var/www/listking/react-listking
npm run build
firefox http://10.98.228.87/react-listking
```

### access dev build Abraxas
```bash
cd /var/www/listking/react-listking
npm start
firefox http://10.98.228.87:3001
```

### access dev build local
```bash
cd ~/workspace/react-kurs/projekt/react-listking
npm start
firefox http://localhost:3001/
```

test user: 
    
    name "user4@example.com"
    password "DHz5fznHskgte3"

url configuration (urls need to be adjusted on different hosts):

    - react-listking/src/assets/base_url.js
    - react-listking/package.json

### run tests

```sh
ssh ubuntu@listking
cd /var/www/listking/react-listking

# run all unit tests in directory src/tests
npm test -p tests
# run unit test in src/tests/components/login.wip.test.js
npm test -p login.wip.test.js
# e2e headless
npx cypress run

# e2e with GUI
#   needs to be executed on desktop system
cat ~/VAULT_GO/keys/ssh-listking-pw | \
sshfs -o password_stdin ubuntu@10.98.228.87:/ ~/workspace/react-kurs/projekt
cd ~/workspace/react-kurs/projekt/var/www/listking/react-listking
npx cypress open
```

### broad todo

- write tests
- add a real database (MongoDB would be a natural choice)
- improve security:
    - see OWASP
    - add HTTPS/TLS

### todo
- implement countListLength() in /lists
- [DONE] implement user ownership of lists
- [DONE] redirect to /lists after login:
    - [DONE] window.location.href
- errors/bugs:
    - [DONE] Button "add list item" doesn't work
    - form validation for login form is missing
    - when trying to load url without being logged in, redirect to login
        - not quite, yet
    - CSS errors
    - [DONE] replace for with htmlFor in Login-component
    - Login-test fails incorrectly if you try to fire multiple input events
- write tests:
    - https://soshace.com/how-to-write-effective-tests-for-react-apps-with-react-testing-library/
    - https://glebbahmutov.com/blog/effective-react-tests/
    - https://wonderproxy.com/blog/automate-selenium-nodejs/
- signup
- [DONE] create install script
- remove debug messages in production build
- improve performance:
    - enable profiler (https://gist.github.com/bvaughn/25e6233aeb1b4f0cdb8d8366e54a3977)
    - [DONE] save more values in browser memory to decrease amount of API requests
- security:
    - see https://www.fortinet.com/resources/cyberglossary/owasp
    - remove database from git repository
    - add HTTPS/TLS
        - register domain
        - acquire TLS cert (-> Let's Encrypt)
        - configure Nginx as TLS termination proxy
