import React, { useEffect } from "react";
// import { BrowserRouter, Route, Router, Redirect, NavLink } from 'react-router-dom'

import {connect} from 'react-redux'                                                                            
import mapStateToProps from '../redux/mapStateToProps'                                                         
import mapDispatchToProps from '../redux/mapDispatchToProps'  

const Logout = ({ logOut }) => {
    
    // const [form, setForm] = useState({
    //     "username": "",
    //     "password": ""
    // })
    useEffect(() => { logOut()})
    
    return(
        <>
            <h2>logged out</h2>
        </>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (Logout);
