import React, { useState, useEffect } from "react";
import {connect} from 'react-redux'                                                                            
import { useParams } from 'react-router-dom'
import { useLocation } from "react-router-dom/cjs/react-router-dom.min";

import mapStateToProps from '../redux/mapStateToProps'                                                         
import mapDispatchToProps from '../redux/mapDispatchToProps'  
import {api} from '../api'
import ListItem from "./ListItem";
// import Logout from "./Logout";

const List = ({ items, loadItems, addItem, logOut, loggedIn,
                currentListId, token }) => {

    const [itemName, setItem] = useState("")
    const listId = Number(useParams().id)
    // let token
    if((token === "" || token === undefined) && loggedIn === true){
        if(localStorage.token){
            token = JSON.parse(localStorage.token)
        }else{
            logOut()
        }
    }
    const userId = localStorage.userId
    
    const location = useLocation()
    let listName = location.state.listName

    useEffect(() => {
        if(loggedIn === false || !localStorage.token){logOut()}
        // console.log("current listName: ", listName)
        loadItems(listId, token, userId, api)
        document.getElementById('item').focus()
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    function handleInput(event) {        
        // console.log("form: ", event.target.name, event.target.value)    
        setItem( event.target.value )
        let elMsg = document.getElementById('submit-feedback')
        if(event.target.value.length > 22){
            elMsg.textContent = 'name too long'
        }else{
            elMsg.textContent = ''
        }
    }
    function handleSubmit(event){
        event.preventDefault()
    }
    function handleEnter(event){
        const key = event.which || event.keyCode || 0;
        console.log("keycode: ", key)
        if(key === 13 || key === 0){
            let elMsg = document.getElementById('submit-feedback')
            console.log(itemName.length)
            if(itemName.length < 23){
                addItem(itemName, currentListId, token, userId, items, api)
                setItem("")
                elMsg.textContent = ''
                document.getElementById('item').focus()
            } else {
                elMsg.textContent = 'name too long'
            }
        }
    }
    return(
        <>
            <h2>{ listName }<span id="counter">{items?.length}</span></h2>
            <ul id="shoppingList">
                { items?.map((item) => {
                    return <ListItem item={item} key={item.id} />    
                    // return <li key={item.id} 
                    //     onClick={(e) => { mark(e, item, token, userId, currentListId, api) }}
                    //     >{item.title}
                    // </li>
                  }
                ) || "no items yet"}
            </ul>
            <div id="new-item" onSubmit={handleSubmit}>
                <input  type="text"      id="item" 
                        value={itemName} name="itemName" 
                        onChange={handleInput}  
                        onKeyPress={handleEnter}
                />
                <div className="button" 
                     type="submit"
                     id="add-item" 
                     onClick={handleEnter}
                >
                    <a className="add">Add list item</a>
                </div>
            </div>
            <div id="submit-feedback"></div>
        </>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (List);
