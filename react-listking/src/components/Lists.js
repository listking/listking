import React, { useState, useEffect } from "react";

import {connect} from 'react-redux'                                                                            
import mapStateToProps from '../redux/mapStateToProps'                                                         
import mapDispatchToProps from '../redux/mapDispatchToProps'  
import {api} from '../api'
// import { Link, useParams } from 'react-router-dom'

import ListListItem from "./ListListItem";

const Lists = ({ lists, loadLists, addList, loadItems, currentListId, 
                 updateListTitle, loggedIn, logOut}) => {

    const [title, setTitle] = useState("")   
    // const token = JSON.parse(localStorage.token)
    let token
    if(loggedIn === true && localStorage.token){
        token = JSON.parse(localStorage.token)
    }else{
        logOut()
    }
    const userId = localStorage.userId

    useEffect(() => {
        if(loggedIn === false || !localStorage.token){
            logOut()
        }
        // console.log("useEffect: " + userId + " " + token)
        loadLists(token, userId, api)
        document.getElementById('item').focus()
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    function handleInput(event) {        
        console.log("form: ", event.target.name, event.target.value)                                                                    
        setTitle( event.target.value )
        let elMsg = document.getElementById('submit-feedback')
        if(event.target.value.length > 22){
            elMsg.textContent = 'name too long'
        }else{
            elMsg.textContent = ''
        }
        // console.log("currentList: ", currentList)
    }
    function handleSubmit(event){
        event.preventDefault()
    }
    function handleSubmitting(event){
        let elMsg = document.getElementById('submit-feedback')
        console.log(title.length)
        if(title.length < 23){
            addList(title, userId, token, lists, api)
            setTitle("")
            elMsg.textContent = ''
            document.getElementById('item').focus()
        } else {
            elMsg.textContent = 'name too long'
        }   
    }
    function handleEnter(event){
        const key = event.which || event.keyCode || 0;
        // console.log("keycode: ", key)
        if(key === 13){
           handleSubmitting(event, title, userId )
        }
    }
    return(
        <>
            <h2>Lists</h2>
            <ul>
                {   lists && lists.map(list => { 
                        return <ListListItem    list={list} 
                                                listname={list.title}
                                                key={list.title} /> 
                    }) 
                }
            </ul>
            <div id="new-item" onSubmit={handleSubmit}>
                <input  type="text"     id="item" 
                        value={title}   name="title" 
                        onChange={handleInput}  
                        onKeyPress={handleEnter}
                />
                <div className="button" 
                     type="submit"
                     id="add-item" 
                     onClick={() => { 
                        handleSubmitting(token, title, userId) 
                     }}
                >
                    <a className="add">Add list</a>
                </div>
            </div>
            <div id="submit-feedback"></div>
        </>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (Lists);
