import React, { useEffect } from "react";

import {connect} from 'react-redux'                                                                            
import mapStateToProps from '../redux/mapStateToProps'                                                         
import mapDispatchToProps from '../redux/mapDispatchToProps'  

import {api} from '../api'

const ListItem = ({ token, item, items, markItem, deleteItem, userId, currentListId }) => {

    // console.log("listitem token: ", token)
    // let item = useSelector((state) => state.item)
    // console.log("listitem: ", item.title)
    // const [sitem, setItem] = useState(item)
    useEffect(() => {
        // console.log("item effect", item)
        // console.log("ListItem items", items)
    }, [])

    function mark(e, item, complete){
        console.log("mark isComplete:", complete)
        item.isComplete = complete
        markItem(item, items, token, userId, currentListId, api)
        document.getElementById('item').focus()
        // let li = e.target.parentElement
        // bool ? li.className = "hot" : li.className = "complete"
        // console.log("marked", li.className)
        // item.title = li.className
    }
    function deleteIt(e, item){
        console.log("delete", item)
        deleteItem(item, currentListId, token, userId, items, api)
        document.getElementById('item').focus()
    }
    return(
        <>{ item.isComplete 
                ? (<li  
                        className="complete" 
                        key={ item.title }
                    >
                        <span   type="button" 
                                onClick={(e) => { mark(e, item, false) }}
                        >{ item.title }</span>
                        <div className="delete"
                             type="button"
                             onClick={(e) => { deleteIt(e, item) }}
                        ></div>
                    </li>) 
                : (<li  type="button" 
                        className="hot" 
                        onClick={(e) => { mark(e, item, true) }}
                        key={item.title}
                    >
                        <span>{ item.title }</span>
                    </li>)
        }</>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (ListItem);
