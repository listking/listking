import React, { useState, useEffect } from "react";
// import { BrowserRouter, Route, Router, Redirect, NavLink, useHistory } from 'react-router-dom'
import {connect} from 'react-redux'                                                                            
import mapStateToProps from '../redux/mapStateToProps'
import mapDispatchToProps from '../redux/mapDispatchToProps'  
import {api} from '../api'

export const Login = ({ loggedIn, logIn }) => {

    const [form, setForm] = useState({
        "username": "",
        "password": ""
    })
    useEffect(() => { }, [loggedIn])
    
    function handleSubmit(event){
        event.preventDefault()
    }
    function handleInput(event) {        
        // console.log("form: ", event.target.name, event.target.value)
        setForm((prevState) => {
            return {
                ...prevState,
                [event.target.name]: event.target.value
            }
        })
    }
    function handleLogin(event){
        console.log("name: ", form.username)
        console.log("passwd: ", form.password)
        // logIn(form) // only use this for debugging tests
        let elMsg = document.getElementById('submit-feedback');
        if(form.username.length >= 5 && form.password.length >= 5){
            elMsg.textContent = ''
            console.log("valid name and pw")
            try{
                logIn(form, api)
                    .catch(error => {console.log("error2")})
            }catch(error){
                console.log("error1")
                elMsg.textContent = "wrong username or password"
            }
        }else if(form.username.length < 5 && form.password.length < 5){
            elMsg.textContent = 'invalid username and password'
        }else if(form.username.length < 5){
            elMsg.textContent = 'invalid username';
        }else if(form.password.length < 5){
            elMsg.textContent = 'invalid password';
        }else{
            elMsg.textContent = 'error'
            console.log("error handling username/password input");
        }
    }
    return(
        <>{ !loggedIn ? (<>
            <h2>Enter account</h2>
            <form id="form" method="post" action="example.com" onSubmit={handleSubmit}>  
                <label htmlFor="username">Enter username: </label>
                <input  type="text" id="username" value={form.username} name="username" 
                        onChange={handleInput} />
                <div id="feedback"></div>
                
                <label htmlFor="password">Enter password: </label>
                <input  type="password" id="password" value={form.password} name="password"
                        onChange={handleInput} />
                
                <input  type="submit" id="submit" value="log in!" 
                        onClick={handleLogin} />
                <div id="submit-feedback"></div>
            </form>
        </>)
        : (<>
            <h2>Welcome!</h2>
            <br></br>
            {/* <div id="submit-feedback">logged in</div> */}
        </>)
        }</>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (Login);
