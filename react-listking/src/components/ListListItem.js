import React, {useState} from "react";
import {connect} from 'react-redux'                           
import {Link} from 'react-router-dom'
                                                 
import mapStateToProps from '../redux/mapStateToProps'                                                         
import mapDispatchToProps from '../redux/mapDispatchToProps'  
import {api} from '../api'

const ListListItem = ({ token, userId, list, lists, deleteList, updateListTitle }) => {

    const [edit, setEdit] = useState(false)
    const [title, setTitle] = useState(list.title)

    function handleSubmit(event){
        event.preventDefault()
    }
    function handleInput(event) {        
        // console.log("form: ", event.target.name, event.target.value)                                                                    
        setTitle( event.target.value )
        // console.log(title)
        let elMsg = document.getElementById('submit-feedback')
        if(event.target.value.length > 22){
            elMsg.textContent = 'name too long'
        }else{
            elMsg.textContent = ''
        }
        // console.log("currentList: ", currentList)
    }
    function handleSubmitting(event){
        let elMsg = document.getElementById('submit-feedback')
        console.log("length ", title.length)
        if(title.length < 23){
            list.title = title
            updateListTitle(list, lists, token, userId)
            setTitle("")
            elMsg.textContent = ''
            document.getElementById('item').focus()
        } else {
            elMsg.textContent = 'name too long'
        }   
    }
    function handleEnter(event, title, listId){
        const key = event.which || event.keyCode || 0;
        // console.log("keycode: ", key)
        if(key === 13){
           handleSubmitting(event, title, listId)
        }
    }    
    function deleteIt(list, token, userId){
        alert(`are you sure you want to delete list '${list.title}'?`)
        const r = window.confirm(`Are you sure you want to delete list '${list.title}'?`);
        if (r === true) {
            console.log("deletion confirmed")
            deleteList(list, lists, token, userId, api)
            document.getElementById('item').focus()
        } else {
            console.log("deletion canceled")
            document.getElementById('item').focus()
        }
    }    
    function editStop(){
        console.log("edit stop")
        setEdit(false)
    }
    function editStart(){
        setEdit(true)
    }
    return(
        <li className="list" 
            id={list.id}
            key={list.id}>
            { edit 
                ? (<>
                    <div className="edit-item" onSubmit={handleSubmit}>
                        <input  type="text"     id="edit-item" 
                                value={title}   name="edit-title" 
                                onChange={handleInput}  
                                onKeyPress={handleEnter}
                        />
                        <span id="edit-stop" onClick={editStop}>stop</span>
                        <div className="button" 
                            type="submit"
                            id="update-item" 
                            onClick={() => { handleSubmitting() }}
                        >
                            <a className="update">update</a>
                        </div>
                    </div>
                    <div id="submit-feedback"></div>
                  </>)
                :(<>
                    <Link to={{
                            pathname: "/list/" + list.id, 
                            state: {listName: list.title}
                        }} >
                    <span type="button" className="listListItem">
                        { list.title }
                    </span>
                    </Link>     
                    <span   className="edit"
                            type="button"  
                            onClick={editStart}>
                        edit
                    </span>
                    <div    className="delete"
                            type="button"
                            onClick={() => { deleteIt(list, token, userId) }}>
                    </div>
                </>)
            }
        </li>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (ListListItem);
