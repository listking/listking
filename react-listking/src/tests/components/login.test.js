
import "@testing-library/jest-dom";
import { render, fireEvent, cleanup, queryByAttribute, waitFor } from '@testing-library/react';

import { createStore, applyMiddleware } from "redux";
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import { initState, reducer } from "../../redux/reducer";

import ConnectedLogin from '../../components/Login';
import Login from "../../components/Login";


describe('<Login />', () => {

    test('initState is set', () => {
        expect(initState.userId).toBe("")
    })
    initState.loggedIn = false
    const renderWithRedux = (
            component,
            { initState, store = createStore(reducer, initState, applyMiddleware(thunk)) } = {}
        ) => {
            return {
                ...render(<Provider store={store}>{component}</Provider>),
                store,
            }
        }
    afterEach(cleanup);

    it('renders login form correctly', () => {
        const { getByLabelText, getByText } = renderWithRedux(<Login />)
        const nameLabel = getByLabelText('Enter username:')
        expect(nameLabel).toBeInTheDocument()
        const pwLabel = getByLabelText('Enter password:')
        expect(pwLabel).toBeInTheDocument()
        const title = getByText('Enter account')
        expect(title).toBeInTheDocument()
        const input = getByLabelText('Enter username:')
        expect(input).toHaveAttribute("type", "text")
    })
    const formSetup = () => {
        const mockLogIn = jest.fn()
        const dom = renderWithRedux(<ConnectedLogin logIn={mockLogIn} />)
        const getById = queryByAttribute.bind(null, 'id');
        let feedback = getById(dom.container, 'submit-feedback')
        let user   = getById(dom.container, 'username')
        let passwd = getById(dom.container, 'password')
        let submit = getById(dom.container, 'submit')
        return { mockLogIn, feedback, user, passwd, submit, dom }
    }
    it('validates form fields correctly 1', () => {
        // both fields invalid
        let { mockLogIn, feedback, submit } = formSetup()
        fireEvent.click(submit)
        expect(feedback).toHaveTextContent('invalid username and password')
        expect(mockLogIn).not.toBeCalled()
    })
    it('validates form fields correctly 2', async() => {
        // username stays empty
        let { mockLogIn, feedback, passwd, submit } = formSetup()
        fireEvent.change(passwd, { target: { value: '12345' } });
        fireEvent.click(submit)
        await waitFor(() => {
            expect(feedback.innerHTML).toEqual('invalid username')
            expect(feedback).toBeInTheDocument()
        })
        expect(mockLogIn).not.toBeCalled()
    })
    it('validates form fields correctly 3', async() => {
        // password stays empty
        let { mockLogIn, feedback, user, submit } = formSetup()
        fireEvent.change(user, { target: { value: '12345' } });
        fireEvent.click(submit)
        await waitFor(() => {
            expect(feedback.innerHTML).toEqual('invalid password')
        })
        expect(mockLogIn).not.toBeCalled()
    })

})