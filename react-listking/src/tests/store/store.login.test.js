import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { initState, reducer } from "../../redux/reducer";
import mapDispatchToProps from '../../redux/mapDispatchToProps'  

describe('store Login', () => {
    
    it('stores user data', async () => {
        const initState = {
            loggedIn: false,
            userId: "",
            token: ""
        }
        const store = createStore(reducer, initState, applyMiddleware(thunk))
        const props = mapDispatchToProps(store.dispatch)
        const form = {
            "username": "user4@example.com",
            "password": "DHz5fznHskgte3"
        }
        const res =  { 
            id: "17", 
            email: "user4@example.com", 
            role: "user", 
            // token is the only value that is not real
            token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZC" 
        }
        const mockApi = {
            login: async function mockLogin(username, password){
                return res
            }
        }
        await props.logIn(form, mockApi)
        console.log("getState: ", store.getState())
        expect(store.getState().userId).toEqual("17")
    })

})