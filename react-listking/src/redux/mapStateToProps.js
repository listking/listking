
const mapStateToProps = (state, props) => {
    return {
        ...state,
        ...props
    }
}
export default mapStateToProps