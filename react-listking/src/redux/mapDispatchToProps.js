// import { Redirect } from 'react-router-dom'
import {cloneDeep} from 'lodash'
import { api } from '../api'

const mapDispatchToProps = (dispatch) => {

    return {
        logIn: (form, api) => {
            // let elMsg = document.getElementById('submit-feedback');
            api.login(form.username, form.password)
                .then(data => {
                    console.log("async data: ", data)
                    dispatch({ type: "LOG_IN", payload: data})
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        logOut: () => {
            dispatch({ type: "LOG_OUT"})
        },
        loadLists: (token, userId, api) => {
            console.log("mapDispatchToProps loadLists", userId, " ", token)
            api.loadLists(token, userId, api)
                .then(data => {
                    dispatch({ type: "LOAD_LISTS", payload: data})
                })
                .catch(error => {
                    console.log("error: ", error)
                    if(error.statusCode === 401){
                        dispatch({ type: "LOG_OUT"})
                    }
                })
        },
        addList: (title, userId, token, lists, api) => {
            console.log("dispatching addList: ", title)
            api.addList(token, title, userId)
                .then(response => {
                    if(response.status === 200){
                        let data = {}
                        lists.push(response.data)
                        data.lists = cloneDeep(lists)
                        console.log("data", data)
                        dispatch({ type: "LOAD_ITEMS", payload: data})
                    }else{
                        let elMsg = document.getElementById('submit-feedback');
                        elMsg.innerHTML = "there was an error"
                    }
                })
                .catch(error => {
                    console.log(error)
                    let elMsg = document.getElementById('submit-feedback');
                    elMsg.innerHTML = "there was an error"
                })
        },
        updateListTitle: (list, lists, token, userId) => {
            console.log("title: " + list.title + ", id: "+  list.id)
            api.updateList(list, token, userId)
                .then(response => {
                    if(response.status === 204){
                        console.log("res", response)
                        let data = {}
                        lists.forEach((el, i) => {
                            if(el.id === list.id){
                                lists[i] = list
                            }
                        })
                        data.lists = cloneDeep(lists)
                        data.token = token
                        data.userId = userId
                        dispatch({ type: "LOAD_LISTS", payload: data})
                    }
                })
                .catch(error => {
                    console.log(error)
                    let elMsg = document.getElementById('submit-feedback');
                    elMsg.innerHTML = "there was an error"
                })
        },
        deleteList: (list, lists, token, userId, api) => {
            console.log("deleting list: ", list)
            console.log("userid ", userId)
            api.deleteList(list, token)
                .then(response => {
                    console.log("res:", response)
                    if(response.status === 204){
                        lists = lists.filter(el => el.id !== list.id)
                        let data = {}
                        data.lists = cloneDeep(lists)
                        data.listId = list.id
                        data.token = token
                        data.userId = userId
                        // console.log(data)
                        dispatch({ type: "LOAD_LISTS", payload: data})
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        },
        loadItems: (listId, token, userId, api) => {
            // loadItemsFunction(listId, token, userId)
            console.log("mapDispatchToProps loadItems")
            api.loadItems(listId, token, userId, api)
                .then(data => {
                    console.log("mapDispatchToProps response: ", data)
                    // console.log("code: ", res.response.status)
                    // data.title = listTitle
                    dispatch({ type: "LOAD_ITEMS", payload: data})
                })
                .catch(error => {
                    // console.log("statuscode: ", error.response.status)
                    if(error.response && error.response.status === 401){
                        dispatch({ type: "LOG_OUT"})
                    }
                })
        },
        addItem: (itemName, listId, token, userId, items, api) => {
            console.log("dispatching item: ", itemName)
            console.log("useerid", userId)
            api.addItem(itemName, listId, token, userId)
                .then(response => {
                    console.log(response)
                    if(response.status === 200){
                        let data = {}
                        // data.item = response.data
                        // items.forEach((el, i) => {
                        //     if(el.id === data.item.id){
                        //         items[i] = data.item
                        //     }
                        // })
                        items.push(response.data)
                        data.items = cloneDeep(items)
                        data.listId = response.data.itemListId
                        console.log("data", data)
                        dispatch({ type: "LOAD_ITEMS", payload: data})
                    }
                })
                .catch(error => {
                    console.log("error:", error)
                    if(error.response.status === 401){
                        // dispatch({ type: "LOG_OUT"})
                        let elMsg = document.getElementById('submit-feedback');
                        elMsg.innerHTML = "unauthorized request"
                    }
                })
        },
        markItem: (item, items, token, userId, listId, api) => {
            // console.log("item mapdispatch:", item)
            console.log("dispatching item(put): ", item)
            api.markItem(item, token, userId)
                .then(response => {
                    console.log("mark", response)
                    if(response.status === 204){
                        let data = {}
                        data.item = item
                        items.forEach((el, i) => {
                            if(el.id === item.id){
                                items[i] = item
                            }
                        })
                        data.items = items
                        data.listId = listId
                        console.log(data)
                        let load = {}
                        load = cloneDeep(data)
                        dispatch({ type: "MARK_ITEM", payload: load})
                    }else{throw console.error()}
                })
                .catch(error => {
                    console.log("error:", error.response.status)
                    if(error.response.status === 401){
                        let elMsg = document.getElementById('submit-feedback');
                        elMsg.innerHTML = "unauthorized request"
                    }
                })
        },
        deleteItem: (item, listId, token, userId, items, api) => {
            console.log("mapDispatch delete: ", item)
            api.deleteItem(item, token)
                .then(response => {
                    console.log("res", response)
                    if(response.status === 204){
                        items = items.filter(el => el.id !== item.id)
                        let data = {}
                        data.items = cloneDeep(items)
                        data.listId = listId
                        // console.log(data)
                        dispatch({ type: "LOAD_ITEMS", payload: data})
                    }
                })
                .catch(error => { 
                    console.log("error:", error)
                    if(error.response.status === 401){
                        // dispatch({ type: "LOG_OUT"})
                        let elMsg = document.getElementById('submit-feedback');
                        elMsg.innerHTML = "unauthorized request"
                    }
                })
       }
    }
}
export default mapDispatchToProps
