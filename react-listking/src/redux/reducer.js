const initState = {
    userId: "",
    token: "",
    lists: [],
    currentListId: 3,
    currentListName: "",
    loggedIn: true,
    items: [],
}
const reducer = (state = initState, action) => {
    switch(action.type){
        case "LOG_IN":  {                                         
            // console.log("reducer payload: ", action.payload.token)
            const token = JSON.stringify(action.payload.token)
            localStorage.setItem("token", token)
            localStorage.setItem("userId", action.payload.id)
            // console.log("logging in storage")
            // console.log("storage ", JSON.parse(localStorage.getItem("token")))
            // window.location.href = window.location.href.slice(0, -5)
            // console.log(window.location.href)
            return {                                                   
                ...state,
                loggedIn: true,
                token: action.payload.token,
                userId: action.payload.id
            }
        }
        case "LOG_OUT":{
            localStorage.removeItem("token")
            return{
                ...state,
                loggedIn: false,
                token: ""
            }
        }
        case "LOAD_LISTS":{
            console.log("lists reducer payload: ", action.payload)
            return{
                ...state,
                lists: action.payload.lists,
                token: action.payload.token,
                userId: action.payload.userId
                // currentListName: liste[0].title
            }
        }
        case "LOAD_ITEMS":{
            console.log("LOAD_ITEMS reducer payload: ", action.payload)
            console.log("state", state)
            return{
                ...state,
                items: action.payload.items,
                currentListId: action.payload.listId,
            }
        }
        case "MARK_ITEM":{
            console.log("successfully marked:", action.payload)
            return{
                ...state,
                // item: action.payload.item,
                items: action.payload.items,
                currentListId: action.payload.listId
            }
        }
        default:
            return state
    }
}
// export default reducer
export { reducer, initState }
