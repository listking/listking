import './App.css';

// import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Redirect, NavLink } from 'react-router-dom'

import {connect} from 'react-redux'
import mapStateToProps from './redux/mapStateToProps'
import mapDispatchToProps from './redux/mapDispatchToProps'

import List from './components/List';
import Lists from './components/Lists';
import Login from './components/Login';
import Logout from './components/Logout';

function App({ loggedIn, currentListId }) {

    return (
        <div id="page">
            <BrowserRouter basename="/react-listking">
                {/* <h1>List King</h1>  we just need h1's background image */}
                <h1></h1>
                <nav>
                    <NavLink to="/lists" exact activeClassName="active">
                        <div>Lists</div>
                    </NavLink>
                    { loggedIn 
                        ? (<> <NavLink to="/logout" exact><div>Logout</div></NavLink>
                              {/* <Redirect to="/lists" /> */}
                          </>) 
                        : ( <> <NavLink to="/login" exact><div>Login</div></NavLink>
                              <Redirect to="/login" />
                            </>)
                    }
                </nav>
                <hr></hr><br></br>
                <Route exact path="/">
                    { loggedIn 
                        ? <Redirect push to="/lists" exact /> 
                        : <Redirect push to="/login" exact />
                    }
                </Route>
                <Route path="/lists" exact >
                    { loggedIn 
                        ? <Lists /> 
                        : <Redirect to="/login" exact /> 
                    }
                    {/* <Lists/> */}
                </Route>
                <Route path="/list/:id" >
                    {loggedIn   
                        ? <List />
                        : <Route path="/login" exact component={Login} />
                    }
                </Route>
                <Route path="/login" exact >
                    { loggedIn 
                        ? <Redirect push to="/lists" exact /> 
                        : <Login />
                    }
                    {/* <Login/> */}
                </Route>
                <Route path="/logout" exact >
                    { loggedIn === true
                        ? <Logout />
                        : <Redirect push to="/login" exact /> 
                    }
                    {/* <Logout/> */}
                </Route>
            </BrowserRouter>
        </div>
    );
}
export default connect(mapStateToProps, mapDispatchToProps) (App);
