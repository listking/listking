import axios from "axios"
import { BACKEND_BASE_URL } from "./assets/base_url";

export const api = {

    login: async function(username, password){   
        console.log("api-test", username, password)
        try{
            let res = await axios.post( BACKEND_BASE_URL + "users/login", {
                email: username,
                password: password
            })
            const token = res.data.token
            const url = BACKEND_BASE_URL + "users/me"
            const header = { headers: { 
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token 
            } }
            try{
                res = await axios.get(url, header)
                console.log("res api: ", res)
                let data = res.data
                data.token = token
                console.log("data: ", data)
                return data
            }catch(error){
                throw error
            }
        }catch(error){
            throw error
        }
    },
    loadLists: async function(token, userId){
        console.log("api.js loadList", userId+token)
        const url = BACKEND_BASE_URL + "users/" + userId + "/item-lists"
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let res = await axios.get(url, header)
            let data = {}
            console.log("data: ", data)
            data.lists = res.data
            data.token = token
            data.userId = userId
            return data
        }catch(error){
        //     console.log(error)
            throw error
        }
    },
    addList: async function(token, title, userId){
        console.log("api.js addList", title)
        const url = BACKEND_BASE_URL + "users/" + userId + "/item-lists"
        const data = {
            "title": title,
            "userId": userId
        }
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let response = await axios.post(url, data, header)
            console.log("res addList", response)
            return response
        }catch(error){
            throw error
        }
    },
    updateList: async function(list, token, userId){
        console.log("api.js updateList", list.title)
        const url = BACKEND_BASE_URL + "item-lists/" + list.id
        const data = {
            "id": list.id,
            "title": list.title,
            "userId": userId
        }
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let response = await axios.put(url, data, header)
            return response
        }catch(error){
            throw error
        }
    },
    deleteList: async function(list, token){
        console.log("api.js deleteList", list.title)
        const url = BACKEND_BASE_URL + "item-lists/" + list.id
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        let response = await axios.delete(url,header)
        return response
    },
    loadItems: async function(listId, token){
        console.log("api loading items")
        const url = BACKEND_BASE_URL + "item-lists/" + listId + "/items"
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let res = await axios.get(url, header)
            // console.log(res)
            let data = {}
            // data.lists = res.lists
            data.items = res.data
            data.listId = listId
            // data.token = token
            // data.userId = userId
            return data
        }catch(error){
            // console.log(error)
            // return error
            throw error
        }
    },          
    markItem: async function(item, token, userId){
        const url = BACKEND_BASE_URL + "items/" + item.id
        const data = {
            "id": item.id,
            "title": item.title,
            "isComplete": item.isComplete,
            "itemListId": item.itemListId
        }
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let response = axios.put(url, data, header)
            console.log("response: ", response)
            return response
        }catch(error){
            console.log(error)
            throw error
        }
    },
    addItem: async function(itemName, listId, token, userId){
        const url = BACKEND_BASE_URL + "item-lists/" + listId + "/items" 
        const data = {
            "title": itemName,
            "itemListId": listId
        }
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let response = await axios.post(url, data, header)
            console.log("res additem", response)
            return response
        }catch(error){
            console.log("error: ", error)
            throw error
        }
    },
    deleteItem: async function(item, token){
        console.log("api.delete", item, token)
        const url = BACKEND_BASE_URL + "items/" + item.id
        const header = { headers: { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token 
        } }
        try{
            let response = axios.delete(url, header)
            console.log("response: ", response)
            return response
        }catch(error){
            console.log(error)
            throw error
        }
    }

}