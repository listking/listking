#!/bin/bash
# 
###    pull changes from dev server and merge them into production build
#
git checkout react-projekt
git pull
git checkout react-scaleway
read -e -p "enter commit id" commit_id
echo "Your commit id was: ${commit_id}"
git merge $commit_id
cp package_scw.json package.json
cp src/assets/base_url_scw.js src/assets/base_url.json
npm run build
