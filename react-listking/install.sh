#!/bin/bash
###   install script for react-listking
#
###   Installs and starts react-listking (react development server) and its backend.
###   Script assumes a fresh install of Ubuntu 20.04 that was recently updated. 
###   Please run as regular user, not root.
#
if (( "$EUID" == 0 ))
  then 
    echo; echo "Please run script as a regular user. Exiting script."; echo
  exit
fi
sudo apt -y install --no-install-recommends npm
sudo apt -y install nginx
sudo npm install -g n
sudo n 12
sudo PATH=$PATH
###   backend
cd ~
git clone -b local https://bitbucket.org/listking/listking-backend.git
cd listking-backend
npm install
npm start &
# test backend install: curl localhost:3000/ping
# browse API: firefox localhost:3000/explorer
#
###   frontend
cd /var
sudo mkdir -p www/listking
sudo chown -R ubuntu:www-data www/listking
cd www/listking
git clone -b react-projekt https://bitbucket.org/listking/listking.git .
sudo cp nginx-site-default_Abraxas /etc/nginx/sites-available/default
sudo service nginx reload
# test frontend: firefox localhost/react-listking
cd react-listking 
sleep 5
echo
read -p "If you want to serve the app with nginx, review and if necessary adjust URLs in 
src/assets/base_url.js and package.json.homepage. Answering 'no' will cancel script. 
Continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "Yes. Continuing install script";;
  n|N ) echo "No. Canceling script";;
  * ) echo "invalid";;
esac
echo 
npm install
npm start