# List King

### What is this?

This is a rebuild of an example app from the book "JavaScript & JQuery" by Jon Duckett (http://www.javascriptbook.com/). It uses assets distributed with the book. The plan is to build a simple app and try out various approaches to implementing the frontend functionality, e.g. with plain JavaScript, JQuery, and a more advanced framework like Vue.js.
Consumes https://bitbucket.org/listking/listking-backend.

See also https://bitbucket.org/listking/listking/src/react-projekt/react-listking/README.md

### What does it do?

Displays a list of items, meant to represent a to do list, shopping list or similar. Load items from database, enter new items, delete items.

### screenshots

login

![login](/screenshots/2021-09-26-153208_631x666_scrot.png?raw=true "login")

list of lists

![lists](/screenshots/2021-09-26-153351_603x637_scrot.png?raw=true "lists")

list of todo-items

![list](/screenshots/2021-09-26-153404_584x844_scrot.png?raw=true "list")

### to do

- [DONE] get a first version running with plain JavaScript and without a backend, based on the first 6 chapters of the book
- [DONE] build a backend with a Node.js framework (https://loopback.io/)
- [DONE] rebuild frontend to actually use the loopback-API, but without authentication
- [DONE] build a new version with React.js
- features:
    - [DONE] simple authentication mechanism (maybe jwt without refresh token, or something else)
    - [DONE] complete authentication mechanism based on jwt-tokens, including refresh tokens
    - [DONE] create/select/delete lists


### development workflow

```bash
cd ~/workspace/listking-backend && npm start        # start API server
npx eslint js/script.js                             # linter
npx node-sass --watch css/c06.scss -o css/          # scss
firefox ~/workspace/listking/index.html             # navigate to site
```
